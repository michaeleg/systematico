﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class Reporte_Factura : Form
    {
        private DataSet dsReporteFactura;
        private BindingSource bsReporteFactura;

        public DataSet DsReporteFactura
        {
            get
            {
                return dsReporteFactura;
            }

            set
            {
                dsReporteFactura = value;
            }
        }

        public Reporte_Factura()
        {
            InitializeComponent();
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvReporte.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder ver", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmReporteFactura frf = new FrmReporteFactura();
            frf.DsSistema = DsReporteFactura;
            frf.DrFactura = drow;
            frf.Show();
        }

        private void Reporte_Factura_Load(object sender, EventArgs e)
        {
            bsReporteFactura.DataSource = DsReporteFactura;
            bsReporteFactura.DataMember = DsReporteFactura.Tables["Reporte"].TableName;
            dgvReporte.DataSource = bsReporteFactura;
            dgvReporte.AutoGenerateColumns = true;
        }
    }
}

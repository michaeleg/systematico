﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblcliente;
        private DataSet dscliente;
        private BindingSource bscliente;
        private DataRow drcliente;
        public FrmCliente()
        {
            InitializeComponent();
            bscliente = new BindingSource();
        }

        public DataRow Drcliente
        {
            set
            {
                drcliente = value;
              
                txtCed.Text = drcliente["Cédula"].ToString();
                txtNombres.Text = drcliente["Nombres"].ToString();
                txtApellidos.Text = drcliente["Apellidos"].ToString();
                txtDireccion.Text = drcliente["Dirección"].ToString();
                txtCelular.Text = drcliente["Teléfono"].ToString();
                txtCelular.Text = drcliente["Celular"].ToString();
                txtDireccion.Text = drcliente["Dirección"].ToString();
                
            }
        }

        public DataSet Dscliente
        {
            get
            {
                return dscliente;
            }

            set
            {
                dscliente = value;
            }
        }

        public DataTable Tblcliente
        {
            get
            {
                return tblcliente;
            }

            set
            {
                tblcliente = value;
            }
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bscliente.DataSource = Dscliente;
            bscliente.DataMember = Dscliente.Tables["cliente"].TableName;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string cedula, nombres, apellidos, direccion, Celular;
         

            
            cedula = txtCed.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            Celular = txtCelular.Text;
         
     


            if (drcliente != null)
            {
                DataRow drNew = Tblcliente.NewRow();

                int index = Tblcliente.Rows.IndexOf(drcliente);
                drNew["Id"] = drcliente["Id"];
              
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["Direccion"] = direccion;
             
                drNew["Celular"] = Celular;
              


                Tblcliente.Rows.RemoveAt(index);
                Tblcliente.Rows.InsertAt(drNew, index);

            }
            else
            {
                Tblcliente.Rows.Add(Tblcliente.Rows.Count + 1, cedula, nombres, apellidos, direccion, Celular);
            }

            Dispose();
        }
    }
}

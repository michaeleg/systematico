﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class Reporte_Empleado : Form
    {
        private DataSet dssistema;
        private DataRow drEmpleado;

        public DataSet Dssistema
        {
            get
            {
                return dssistema;
            }

            set
            {
                dssistema = value;
            }
        }

        public DataRow DrEmpleado
        {
            get
            {
                return drEmpleado;
            }

            set
            {
                drEmpleado = value;
            }
        }

        public Reporte_Empleado()
        {
            InitializeComponent();
        }

        private void Reporte_Empleado_Load(object sender, EventArgs e)
        {
            dssistema.Tables["ReporteEmpleado"].Rows.Clear();
            DataTable dtEmpleado = dssistema.Tables["Empleado"];
            int countEmpleado = dtEmpleado.Rows.Count;

            DataRow drEmpleado = dtEmpleado.Rows[countEmpleado - 1];
            DataRow drRemporteEmpleado = dssistema.Tables["ReporteEmpleado"].Rows.Find(drEmpleado["Empleado"]);
            DataTable dtDetalleFactura = dssistema.Tables["DetalleFactura"];

            DataRow[] drDetallesFacturas =
                dtDetalleFactura.Select(String.Format("Factura = {0}",
                drRemporteEmpleado["Id"]));
            foreach (DataRow dr in drDetallesFacturas)
            {
                DataRow drReporteFactura = dssistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dssistema.Tables["Producto"].Rows.Find(dr["Producto"]);
                drReporteFactura["Cod_Factura"] = dr["CodFactura"];
                drReporteFactura["Fecha"] = drReporteFactura["Fecha"];
                drReporteFactura["Subtotal"] = drReporteFactura["Subtotal"];
                drReporteFactura["IVA"] = drReporteFactura["Iva"];
                drReporteFactura["Total"] = dr["Total"];
           

                dssistema.Tables["ReporteFactura"].Rows.Add(drReporteFactura);
            }


            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.ReporteFactura.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteEmpleado", dssistema.Tables["ReporteEmpleado"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);
            this.reportViewer1.RefreshReport();

        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}

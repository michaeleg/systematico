﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFacturaEmpleado
    {
        
        private string cod_factura;
        private DateTime fecha;
        private int id_empleado;
        private double subtotal;
        private double iva;
        private double total;

        public ReporteFacturaEmpleado(string cod_factura, DateTime fecha, int id_empleado, double subtotal, double iva, double total)
        {
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.id_empleado = id_empleado;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public int Id_empleado
        {
            get
            {
                return id_empleado;
            }

            set
            {
                id_empleado = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }
    }
}

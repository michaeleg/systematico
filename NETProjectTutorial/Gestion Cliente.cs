﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class Gestion_Cliente : Form
    {
        private DataSet dscliente;
        private BindingSource bscliente;

        public DataSet Dscliente
        {
            get
            {
                return dscliente;
            }

            set
            {
                dscliente = value;
            }
        }

        public Gestion_Cliente()
        {
            InitializeComponent();
            bscliente = new BindingSource();
        }

      
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fp = new FrmCliente();
            fp.Tblcliente = Dscliente.Tables["cliente"];
            fp.Dscliente = Dscliente;
            fp.ShowDialog();
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bscliente.Filter = string.Format("Cédula like '*{0}*' or Nombres like '*{0}*' or Apellidos like '*{0}*'", txtbuscar.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Gestion_Cliente_Load(object sender, EventArgs e)
        {
            bscliente.DataSource = Dscliente;
            bscliente.DataMember = Dscliente.Tables["cliente"].TableName;
            dgvcliente.DataSource = bscliente;
            dgvcliente.AutoGenerateColumns = true;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvcliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila ", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fp = new FrmCliente();
            fp.Tblcliente = Dscliente.Tables["cliente"];
            fp.Dscliente = Dscliente;
            fp.Drcliente = drow;
            fp.ShowDialog();
        }
    }
}
